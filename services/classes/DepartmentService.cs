using System;
using api.Models;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using api.Services.Interfaces;

namespace api.Services
{
    public class DepartmentService : IService<Department>
    {
        public IEnumerable<Department> GetAll()
        {
            return Enumerable.Range(1, 5).Select(index => new Department
                {
                    Id = index,
                    Name = $"Department {index}",
                    Address = $"{index * 27} Avenue {index * 2}"
                })
                .ToArray();
        }

        public IList<Department> ListAll()
        {
            return Enumerable.Range(1, 5).Select(index => new Department
                {
                    Id = index,
                    Name = $"Department {index}",
                    Address = $"{index * 27} Avenue {index * 2}"
                }).ToList();
        }
    }
}