using System;
using api.Models;
//using System.Linq;
using System.Collections.Generic;
using api.Services.Interfaces;

namespace api.Services
{
    public class EmployeeService : IService<Employee>
    {
        private IList<Employee> employees;
        public EmployeeService()
        {
            var salesDepartment = new Department() {Name = "Sales", Id = 1};
            this.employees = new List<Employee>() {
                new Employee() {
                    Id = 1,
                    FirstName ="Michael",
                    LastName="Scott",
                    JobTitle="Branch Manager",
                    MailingAddress="89 Dummy avenue",
                    WorksAt = new Department() {Name = "Management", Id = 2}
                },
                new Employee() {
                    Id = 2,
                    FirstName ="Dwight",
                    LastName="Schrute",
                    JobTitle="Assistant Regional Manager",
                    MailingAddress="Schrute Farms",
                    WorksAt = salesDepartment
                },
                new Employee() {
                    Id = 3,
                    FirstName ="Jim",
                    LastName="Halpert",
                    JobTitle="Salesman",
                    MailingAddress="54b, 27th street",
                    WorksAt = salesDepartment
                },
                new Employee() {
                    Id = 4,
                    FirstName ="Pam",
                    LastName="Beesly",
                    JobTitle="Recepcionist",
                    MailingAddress="96 close to Jim",
                    WorksAt = new Department() {Name = "Administration", Id = 3}
                }
            };
        }
        public IEnumerable<Employee> GetAll()
        {
            return this.employees;
            // return Enumerable.Range(1, 5).Select(index => new Employee
            //     {
            //         Name = $"Employee {index}",
            //         Address = $"{index * 27} Avenue {index * 2}"
            //     })
            //     .ToArray();
        }
        public IList<Employee> ListAll()
        {
            return this.employees;
        }
    }
}