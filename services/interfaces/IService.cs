using System;
using System.Collections;
using System.Collections.Generic;
namespace api.Services.Interfaces
{
    public interface IService<T> where T:class
    {
        IEnumerable<T> GetAll();
        IList<T> ListAll();
        /* T Save(T t);
        T Delete(T t);
        T Update(T t);
        T GetById(object id);
        T GetByParam(object param);*/
    }

}