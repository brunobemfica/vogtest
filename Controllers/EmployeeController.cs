using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using api.Models;
using api.Services;
namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly ILogger<EmployeeController> _logger;
        private readonly EmployeeService _empservice;

        public EmployeeController(ILogger<EmployeeController> logger, EmployeeService empservice)
        {
            _logger = logger;
            _empservice = empservice;
        }

        [HttpGet]
        public IEnumerable<Employee> Get()
        {
            return _empservice.GetAll();
        }

        [HttpGet]
        [Route("department/{departmentId}")]
        public IEnumerable<Employee> GetByDepartmentId(int departmentId)
        {
            return _empservice.GetAll().Where<Employee>(e => e.WorksAt.Id == departmentId);
        }
    }
}
