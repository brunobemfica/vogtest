using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using api.Models;
using api.Services;
namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DepartmentController : ControllerBase
    {
        private readonly ILogger<DepartmentController> _logger;
        private readonly DepartmentService _depservice;

        public DepartmentController(ILogger<DepartmentController> logger, DepartmentService depservice)
        {
            _logger = logger;
            _depservice = depservice;
        }

        [HttpGet]
        public IEnumerable<Department> Get()
        {
            return _depservice.GetAll();
        }
    }
}
