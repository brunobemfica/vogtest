FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 59518
EXPOSE 44364

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY api/api.csproj api/
RUN dotnet restore api/api.csproj
COPY . .
WORKDIR /src/api
RUN dotnet build api.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish api.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "api.dll"]